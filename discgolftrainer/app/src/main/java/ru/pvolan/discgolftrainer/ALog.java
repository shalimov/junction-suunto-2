package ru.pvolan.discgolftrainer;

import android.util.Log;

public class ALog {

    public static void log(String text){
        Log.i("DiscTrainer", text);
    }

    public static void log(Throwable throwable) {
        Log.i("DiscTrainer", throwable.toString());
        for (StackTraceElement stackTraceElement : throwable.getStackTrace()) {
            Log.i("DiscTrainer", stackTraceElement.toString());
        }
    }
}
