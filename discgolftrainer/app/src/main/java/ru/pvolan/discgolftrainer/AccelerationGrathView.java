package ru.pvolan.discgolftrainer;

import android.content.Context;
import android.graphics.Canvas;
import android.os.Build;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.annotation.StyleRes;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import java.util.ArrayList;
import java.util.List;

import rx.functions.Action;

public class AccelerationGrathView extends FrameLayout {

    public AccelerationGrathView(Context context) {
        super(context);
        init();
    }

    public AccelerationGrathView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public AccelerationGrathView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public AccelerationGrathView(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr, @StyleRes int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }


    private List<Measurement> data = new ArrayList<>();

    private void init() {

    }


    public void setData(List<Measurement> data) {
        this.data = data;
        invalidate();
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if(data.size() < 1) return;


        int w = getWidth();
        int h = getHeight();


    }


    private void drawGrath(GetData dataGetter, int left, int top, int right, int bottom){


    }


    interface GetData{

        float getData(Measurement m);

    }

}
