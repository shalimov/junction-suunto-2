package ru.pvolan.discgolftrainer;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.movesense.mds.Mds;
import com.movesense.mds.MdsException;
import com.movesense.mds.MdsNotificationListener;
import com.movesense.mds.MdsSubscription;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static ru.pvolan.discgolftrainer.MainActivity.URI_EVENTLISTENER;

public class MonitoringActivity extends AppCompatActivity {


    public static final String SERIAL = "serial";
    private MdsSubscription mdsSubscription;

    public static void startActivity(Context context, String serial) {
        Intent intent = new Intent(context, MonitoringActivity.class);
        intent.putExtra(SERIAL, serial);
        context.startActivity(intent);
    }


    View buttonStartThrow;
    View buttonEndThrow;
    Network network;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_monitoring);

        network = new Network(this);

        buttonStartThrow = findViewById(R.id.buttonStartThrow);
        buttonEndThrow = findViewById(R.id.buttonEndThrow);

        buttonStartThrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonStartThrowClick();
            }
        });

        buttonEndThrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonEndThrowClick();
            }
        });


        mdsSubscription = Mds.builder().build(this).subscribe(URI_EVENTLISTENER,
                FormatHelper.formatContractToJson(getIntent().getStringExtra(SERIAL), "Meas/Acc/52"), new MdsNotificationListener() {
                    @Override
                    public void onNotification(String data) {

                        LinearAcceleration linearAccelerationData = new Gson().fromJson(
                                data, LinearAcceleration.class);

                        if(throwAccelerations != null){
                            throwAccelerations.add(linearAccelerationData);
                            ALog.log("Add data pack");
                        }

/*
                        if (linearAccelerationData != null) {

                            LinearAcceleration.Array arrayData = linearAccelerationData.body.array[0];

                            xAxisTextView.setText(String.format(Locale.getDefault(),
                                    "x: %.6f", arrayData.x));
                            yAxisTextView.setText(String.format(Locale.getDefault(),
                                    "y: %.6f", arrayData.y));
                            zAxisTextView.setText(String.format(Locale.getDefault(),
                                    "z: %.6f", arrayData.z));

                            try {
                                seriesX.appendData(
                                        new DataPoint(linearAccelerationData.body.timestamp, arrayData.x), false,
                                        200);
                                seriesY.appendData(
                                        new DataPoint(linearAccelerationData.body.timestamp, arrayData.y), true,
                                        200);
                                seriesZ.appendData(
                                        new DataPoint(linearAccelerationData.body.timestamp, arrayData.z), true,
                                        200);
                            } catch (IllegalArgumentException e) {
                                Log.e(LOG_TAG, "GraphView error ", e);
                            }
                        }*/
                    }

                    @Override
                    public void onError(MdsException error) {
                        ALog.log("onError(): " + error);
/*
                        Toast.makeText(LinearAccelerationTestActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                        buttonView.setChecked(false);*/
                    }
                });
    }



    private List<LinearAcceleration> throwAccelerations;



    private void buttonStartThrowClick() {
        buttonStartThrow.setVisibility(View.GONE);
        buttonEndThrow.setVisibility(View.VISIBLE);
        throwAccelerations = new ArrayList<>();

    }


    private void buttonEndThrowClick() {
        ALog.log("Measured " + throwAccelerations.size());

        buttonStartThrow.setVisibility(View.VISIBLE);
        buttonEndThrow.setVisibility(View.GONE);

        List<Measurement> measurements = new ArrayList<>();

        if(throwAccelerations.size() > 1){

            long startTimestamp = throwAccelerations.get(0).body.timestamp;

            for (int i = 0; i < throwAccelerations.size()-1; i++) {
                LinearAcceleration currentAcceleration = throwAccelerations.get(i);
                LinearAcceleration nextAcceleration = throwAccelerations.get(i+1);

                long relativeTimestamp = currentAcceleration.body.timestamp - startTimestamp;
                long gap = nextAcceleration.body.timestamp - currentAcceleration.body.timestamp;

                LinearAcceleration.Array[] arrays = currentAcceleration.body.array;

                double oneStepGap = (gap/(double)arrays.length);


                for (int i1 = 0; i1 < arrays.length; i1++) {
                    LinearAcceleration.Array linearAccData = arrays[i1];

                    measurements.add(new Measurement(
                            (long) (relativeTimestamp + (oneStepGap * i1)),
                            linearAccData.x,
                            linearAccData.y,
                            linearAccData.z
                    ));
                }
            }
        }

        for (Measurement measurement : measurements) {
            ALog.log(measurement.toString());
        }

        ThrowData throwData = new ThrowData(
                getIntent().getStringExtra(SERIAL),
                Calendar.getInstance(),
                measurements
        );

        postThrowDataAsync(throwData);

        ALog.log("Measurment completed");

        throwAccelerations = null;
    }


    void postThrowDataAsync(ThrowData throwData){
        new AsyncTask<ThrowData, Void, Void>(){

            @Override
            protected Void doInBackground(ThrowData... params) {
                try {
                    network.postMeasurements(throwData);
                } catch (Exception e) {
                    ALog.log(e);
                    Toast.makeText(MonitoringActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                }
                return null;
            }
        }
        .execute(throwData);
    }


    @Override
    protected void onDestroy() {
        if(mdsSubscription != null){
            mdsSubscription.unsubscribe();
            mdsSubscription = null;
        }
        super.onDestroy();
    }
}




class ThrowData{

    public final String deviceId;
    public final Calendar timestamp;
    public final List<Measurement> measurements;

    public ThrowData(String deviceId, Calendar timestamp, List<Measurement> measurements) {
        this.deviceId = deviceId;
        this.timestamp = timestamp;
        this.measurements = measurements;
    }
}



class Measurement {
    public Measurement(long timestamp, double xAcc, double yAcc, double zAcc) {
        this.timestamp = timestamp;
        this.xAcc = xAcc;
        this.yAcc = yAcc;
        this.zAcc = zAcc;
    }

    public final long timestamp;
    public final double xAcc;
    public final double yAcc;
    public final double zAcc;

    @Override
    public String toString() {
        return "Measurement{" +
                "timestamp=" + timestamp +
                ", xAcc=" + xAcc +
                ", yAcc=" + yAcc +
                ", zAcc=" + zAcc +
                '}';
    }
}



class LinearAcceleration {

    @SerializedName("Body")
    public final Body body;

    public LinearAcceleration(Body body) {
        this.body = body;
    }

    public static class Body {
        @SerializedName("Timestamp")
        public final long timestamp;

        @SerializedName("ArrayAcc")
        public final Array[] array;

        @SerializedName("Headers")
        public final Headers header;

        public Body(long timestamp, Array[] array, Headers header) {
            this.timestamp = timestamp;
            this.array = array;
            this.header = header;
        }
    }

    public static class Array {
        @SerializedName("x")
        public final double x;
        @SerializedName("y")

        public final double y;
        @SerializedName("z")
        public final double z;

        public Array(double x, double y, double z) {
            this.x = x;
            this.y = y;
            this.z = z;
        }
    }

    public static class Headers {
        @SerializedName("Param0")
        public final int param0;

        public Headers(int param0) {
            this.param0 = param0;
        }
    }
}

