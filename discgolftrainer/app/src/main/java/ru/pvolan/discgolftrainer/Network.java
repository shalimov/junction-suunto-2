package ru.pvolan.discgolftrainer;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;

public class Network {


    private Context context;

    public Network(Context context) {
        this.context = context;
    }

    public void postMeasurements(ThrowData throwData) throws Exception {
        JSONObject jsonObject = new JSONObject();


        ALog.log("preparing json");

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

        jsonObject.put("timestamp", dateFormat.format(throwData.timestamp.getTime()));
        jsonObject.put("deviceId", throwData.deviceId);

        JSONArray measurementsJson = new JSONArray();
        for (Measurement measurement : throwData.measurements) {
            JSONObject mJ = new JSONObject();
            mJ.put("milliseconds", measurement.timestamp);

            JSONObject aJ = new JSONObject();
            aJ.put("x", measurement.xAcc);
            aJ.put("y", measurement.yAcc);
            aJ.put("z", measurement.zAcc);

            mJ.put("acceleration", aJ);

            measurementsJson.put(mJ);
        }

        jsonObject.put("sensorData", measurementsJson);


        ALog.log(jsonObject.toString());
        ALog.log("sending some staff");

        //post actually

        URL url = new URL("http://10.100.39.118:8080/dump");
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setRequestMethod("POST");
        urlConnection.setRequestProperty("content-type","application/json");

        try {

            urlConnection.setDoOutput(true);
            urlConnection.setDoInput(true);
            OutputStream outputStream = urlConnection.getOutputStream();

            byte[] data = jsonObject.toString().getBytes("utf-8");
            ALog.log("sending " + data.length + " bytes");
            outputStream.write(data);

            outputStream.close();

            int statusCode = urlConnection.getResponseCode();
            InputStream is;
            if(statusCode == 200){
                is = urlConnection.getInputStream();
            } else {
                is = urlConnection.getErrorStream();
            }
            String s = readAll(is);

            ALog.log("read some data " + s);




        } finally {
            urlConnection.disconnect();
        }

        ALog.log("sent some staff");

    }


    private String readAll(InputStream inputStream) throws IOException {
        int count;
        byte[] buffer = new byte[1024];
        ByteArrayOutputStream byteStream =
                new ByteArrayOutputStream(inputStream.available());

        while (true) {
            count = inputStream.read(buffer);
            if (count <= 0)
                break;
            byteStream.write(buffer, 0, count);
        }

        String string = byteStream.toString("utf-8");
        return string;
    }

}
