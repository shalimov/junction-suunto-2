package ru.pvolan.discgolftrainer;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.movesense.mds.Mds;
import com.movesense.mds.MdsConnectionListener;
import com.movesense.mds.MdsException;
import com.movesense.mds.MdsResponseListener;
import com.polidea.rxandroidble.RxBleClient;
import com.polidea.rxandroidble.RxBleDevice;
import com.polidea.rxandroidble.scan.ScanResult;
import com.polidea.rxandroidble.scan.ScanSettings;

import java.util.ArrayList;
import java.util.List;

import rx.Subscription;

public class MainActivity extends AppCompatActivity  {
    private static final String LOG_TAG = MainActivity.class.getSimpleName();
    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 1;

    // MDS
    private Mds mMds;
    public static final String URI_CONNECTEDDEVICES = "suunto://MDS/ConnectedDevices";
    public static final String URI_EVENTLISTENER = "suunto://MDS/EventListener";
    public static final String SCHEME_PREFIX = "suunto://";

    // BleClient singleton
    static private RxBleClient mBleClient;
    private View buttonScan;

    private DevicesAdapter devicesListAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonScan = findViewById(R.id.buttonScan);

        devicesListAdapter = new DevicesAdapter(this);
        ((ListView)findViewById(R.id.listScanResult)).setAdapter(devicesListAdapter);

        mBleClient = RxBleClient.create(this);
        mMds = Mds.builder().build(this);
    }


    @Override
    protected void onPause() {
        super.onPause();

        if(mScanSubscription != null){
            mScanSubscription.unsubscribe();
            mScanSubscription = null;

            buttonScan.setVisibility(View.VISIBLE);
        }
    }

    Subscription mScanSubscription;
    public void onScanClicked(View view) {
        findViewById(R.id.buttonScan).setVisibility(View.GONE);


        mScanSubscription = mBleClient.scanBleDevices(
                new ScanSettings.Builder()
                        // .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY) // change if needed
                        // .setCallbackType(ScanSettings.CALLBACK_TYPE_ALL_MATCHES) // change if needed
                        .build()
                // add filters if needed
        )
                .subscribe(
                        scanResult -> {

                            // Process scan result here. filter movesense devices.
                            if (scanResult.getBleDevice()!=null &&
                                    scanResult.getBleDevice().getName() != null &&
                                    scanResult.getBleDevice().getName().startsWith("Movesense")) {

                                onDeviceFound(scanResult);


/*
                                // replace if exists already, add otherwise
                                MyScanResult msr = new MyScanResult(scanResult);
                                if (mScanResArrayList.contains(msr))
                                    mScanResArrayList.set(mScanResArrayList.indexOf(msr), msr);
                                else
                                    mScanResArrayList.add(0, msr);

                                mScanResArrayAdapter.notifyDataSetChanged();*/
                            }
                        },
                        throwable -> {
                            Log.e(LOG_TAG,"scan error: " + throwable);
                            // Handle an error here.

                            // Re-enable scan buttons, just like with ScanStop
                            //onScanStopClicked(null);
                        }
                );
    }

    private void onDeviceFound(ScanResult scanResult) {
        ALog.log("DEVICE FOUND -------------" + scanResult);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                devicesListAdapter.addResult(scanResult);
            }
        });


    }



/*
    void showDeviceInfo(final String serial) {
        String uri = SCHEME_PREFIX + serial + "/Info";
        final Context ctx = this;
        mMds.get(uri, null, new MdsResponseListener() {
            @Override
            public void onSuccess(String s) {
                Log.i(LOG_TAG, "Device " + serial + " /info request succesful: " + s);
                // Display info in alert dialog
                AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
                builder.setTitle("Device info:")
                        .setMessage(s)
                        .show();
            }

            @Override
            public void onError(MdsException e) {
                Log.e(LOG_TAG, "Device " + serial + " /info returned error: " + e);
            }
        });
    }*/


    private void connectBLEDevice(RxBleDevice bleDevice) {

        ALog.log("Connecting to BLE device: " + bleDevice.getMacAddress());
        mMds.connect(bleDevice.getMacAddress(), new MdsConnectionListener() {

            @Override
            public void onConnect(String s) {
                Log.d(LOG_TAG, "onConnect:" + s);
            }

            @Override
            public void onConnectionComplete(String macAddress, String serial) {
                ALog.log("onConnectionComplete:" + serial);
                MonitoringActivity.startActivity(MainActivity.this, serial);
            }

            @Override
            public void onError(MdsException e) {
                ALog.log("onError:" + e);

                showConnectionError(e);
            }

            @Override
            public void onDisconnect(String bleAddress) {
                ALog.log("onDisconnect: " + bleAddress);
            }
        });
    }

    private void showConnectionError(MdsException e) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle("Connection Error:")
                .setMessage(e.getMessage());

        builder.create().show();
    }


    public void onDeviceClick(ScanResult scanResult) {
        Toast.makeText(this, scanResult.getBleDevice().getName() +  " selected", Toast.LENGTH_LONG).show();
        connectBLEDevice(scanResult.getBleDevice());
    }
}



class DevicesAdapter extends BaseAdapter {

    private List<ScanResult> devices = new ArrayList<>();
    MainActivity mainActivity;

    public DevicesAdapter(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Override
    public int getCount() {
        return devices.size();
    }

    @Override
    public Object getItem(int position) {
        return devices.get(position);
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if(v == null){
            v = new TextView(parent.getContext());
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = (int) v.getTag();
                    mainActivity.onDeviceClick(devices.get(pos));
                }
            });
        }

        final RxBleDevice bleDevice = devices.get(position).getBleDevice();
        ((TextView)v).setText(bleDevice.getName() + " " + bleDevice.getMacAddress());
        ((TextView) v).setMinHeight((int) (50*(mainActivity.getResources().getDisplayMetrics().density)));
        v.setTag(position);
        return v;
    }


    public void addResult(ScanResult r){
        for (ScanResult device : devices) {
            if(device.getBleDevice().getMacAddress().equals(r.getBleDevice().getMacAddress())) return;
        }
        devices.add(r);
        notifyDataSetChanged();
    }
}